
#include "BioEnemyShip.h"


BioEnemyShip::BioEnemyShip()
{
	SetSpeed(150);
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}


void BioEnemyShip::Update(const GameTime *pGameTime)
{
	if (IsActive())
	{
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex()); //Get the time since the game started
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f; //Get speed of ship and space ship will fly back and forth
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed()); //Ship falling down screen during the time it's on screen

		if (!IsOnScreen()) Deactivate(); //If ship isn't on screen, deactivate
	}

	EnemyShip::Update(pGameTime);
}


void BioEnemyShip::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
	}
}
